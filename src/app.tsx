import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

/** Import top-level components **/

/** Import views **/

export var App = () => (
  <Router>
    <div>
      <Switch>
        <Route path='/' component={() => <h2>HomeComponent</h2>} />
      </Switch>
    </div>
  </Router>
);
